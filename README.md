# Statistical Learning

Repo for following Statistical Learning course by Stanford

Course page: https://lagunita.stanford.edu/courses/HumanitiesSciences/StatLearning/Winter2016/about

Follow course: https://lagunita.stanford.edu/courses/HumanitiesSciences/StatLearning/Winter2016/courseware/

My repo on Bitbucket: https://bitbucket.org/hannarud/statlearning

ISL book: http://www-bcf.usc.edu/~gareth/ISL/

ESL book: https://web.stanford.edu/~hastie/ElemStatLearn/
